"use strict";

var gulp = require("gulp"),
  sass = require("gulp-sass"),
  del = require("del"),
  uglify = require("gulp-uglify"),
  cleanCSS = require("gulp-clean-css"),
  rename = require("gulp-rename"),
  merge = require("merge-stream"),
  htmlreplace = require("gulp-html-replace"),
  autoprefixer = require("gulp-autoprefixer"),
  browserSync = require("browser-sync").create();

// Clean task
gulp.task("clean", function() {
  return del(["dist", "assets_front/css/app.css"]);
});

// Copy third party libraries from node_modules into /vendor
gulp.task("vendor:js", function() {
  return gulp
    .src([
      "./node_modules/bootstrap/dist/js/*",
      "./node_modules/jquery/dist/*",
      "!./node_modules/jquery/dist/core.js",
      "./node_modules/popper.js/dist/umd/popper.*"
    ])
    .pipe(gulp.dest("./assets_front/js/vendor"));
});

// Copy font-awesome from node_modules into /fonts
gulp.task("vendor:fonts", function() {
  return gulp
    .src([
      "./node_modules/@fortawesome/fontawesome-free/**/*",
      "!./node_modules/@fortawesome/fontawesome-free/{less,less/*}",
      "!./node_modules/@fortawesome/fontawesome-free/{scss,scss/*}",
      "!./node_modules/@fortawesome/fontawesome-free/.*",
      "!./node_modules/@fortawesome/fontawesome-free/*.{txt,json,md}"
    ])
    .pipe(gulp.dest("./assets_front/fonts/font-awesome"));
});

// vendor task
gulp.task("vendor", gulp.parallel("vendor:fonts", "vendor:js"));

// Copy vendor's js to /dist
gulp.task("vendor:build", function() {
  var jsStream = gulp
    .src([
      "./assets_front/js/vendor/bootstrap.bundle.min.js",
      "./assets_front/js/vendor/jquery.slim.min.js",
      "./assets_front/js/vendor/popper.min.js"
    ])
    .pipe(gulp.dest("./dist/assets_front/js/vendor"));
  var fontStream = gulp
    .src(["./assets_front/fonts/font-awesome/**/*.*"])
    .pipe(gulp.dest("./dist/assets_front/fonts/font-awesome"));
  return merge(jsStream, fontStream);
});

// Copy Bootstrap SCSS(SASS) from node_modules to /assets_front/scss/bootstrap
gulp.task("bootstrap:scss", function() {
  return gulp
    .src(["./node_modules/bootstrap/scss/**/*"])
    .pipe(gulp.dest("./assets_front/scss/bootstrap"));
});

// Compile SCSS(SASS) files
gulp.task(
  "scss",
  gulp.series("bootstrap:scss", function compileScss() {
    return gulp
      .src(["./assets_front/scss/*.scss"])
      .pipe(
        sass
          .sync({
            outputStyle: "expanded"
          })
          .on("error", sass.logError)
      )
      .pipe(autoprefixer())
      .pipe(gulp.dest("./assets_front/css"));
  })
);

// Minify CSS
gulp.task(
  "css:minify",
  gulp.series("scss", function cssMinify() {
    return gulp
      .src("./assets_front/css/app.css")
      .pipe(cleanCSS())
      .pipe(
        rename({
          suffix: ".min"
        })
      )
      .pipe(gulp.dest("./dist/assets_front/css"))
      .pipe(browserSync.stream());
  })
);

// Minify Js
gulp.task("js:minify", function() {
  return gulp
    .src(["./assets_front/js/app.js"])
    .pipe(uglify())
    .pipe(
      rename({
        suffix: ".min"
      })
    )
    .pipe(gulp.dest("./dist/assets_front/js"))
    .pipe(browserSync.stream());
});

// Replace HTML block for Js and Css file upon build and copy to /dist
gulp.task("replaceHtmlBlock", function() {
  return gulp
    .src(["*.html"])
    .pipe(
      htmlreplace({
        js: "assets_front/js/app.min.js",
        css: "assets_front/css/app.min.css"
      })
    )
    .pipe(gulp.dest("dist/"));
});

// Configure the browserSync task and watch file path for change
gulp.task("watch", function browserDev(done) {
  gulp.watch(
    [
      "assets_front/scss/*.scss",
      "assets_front/scss/**/*.scss",
      "!assets_front/scss/bootstrap/**"
    ],
    gulp.series("css:minify")
  );
  gulp.watch(
    "assets_front/js/app.js",
    gulp.series("js:minify")
  );
  done();
});

// Build task
gulp.task(
  "build",
  gulp.series(
    gulp.parallel("css:minify", "js:minify", "vendor"),
    "vendor:build",
    function copyAssets() {
      return gulp
        .src(["*.html", "favicon.ico", "assets_front/img/**"], { base: "./" })
        .pipe(gulp.dest("dist"));
    }
  )
);

// Default task
gulp.task("default", gulp.series("clean", "build", "replaceHtmlBlock"));
