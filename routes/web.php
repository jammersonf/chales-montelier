<?php

Route::name('index')->get('/', 'HomeController@index');
Route::name('terms')->get('/terms', 'HomeController@terms');
Route::name('contact')->post('contact', 'ContactController@store');
Route::group(['prefix' => 'laravel-filemanager'], function () {
    \UniSharp\LaravelFilemanager\Lfm::routes();
});
Auth::routes(['register' => false]);

Route::prefix('admin')->name('adm.')->group(function () {
    Route::get('/', 'AdminController@panel')->name('panel');

    Route::prefix('home')->name('home.')->group(function () {
        Route::get('/', 'AdminController@index')->name('index');
        Route::get('create', 'AdminController@create')->name('create');
        Route::post('create', 'AdminController@store')->name('store');
        Route::post('{id}/delete', 'AdminController@destroy')->name('destroy');
        Route::get('{id}', 'AdminController@show')->name('show');
        Route::post('{id}', 'AdminController@update')->name('update');
    });

    Route::prefix('home2')->name('home2.')->group(function () {
        Route::get('/', 'HomeController@admin')->name('index');
        Route::get('create', 'HomeController@create')->name('create');
        Route::post('create', 'HomeController@store')->name('store');
        Route::post('{id}/delete', 'HomeController@destroy')->name('destroy');
        Route::get('{id}', 'HomeController@show')->name('show');
        Route::post('{id}', 'HomeController@update')->name('update');

        Route::get('{home}/photos/', 'HomePhotoController@index')->name('photo.index');
        Route::get('{home}/photos/create', 'HomePhotoController@create')->name('photo.create');
        Route::post('{home}/photos/create', 'HomePhotoController@store')->name('photo.store');
        Route::post('{home}/photos/{id}/delete', 'HomePhotoController@destroy')->name('photo.destroy');
        Route::get('{home}/photos/{id}', 'HomePhotoController@show')->name('photo.show');
        Route::post('{home}/photos/{id}', 'HomePhotoController@update')->name('photo.update');
    });

    Route::prefix('obras')->name('cons.')->group(function () {
        Route::get('/', 'ConsController@index')->name('index');
        Route::get('create', 'ConsController@create')->name('create');
        Route::post('create', 'ConsController@store')->name('store');
        Route::post('{id}/delete', 'ConsController@destroy')->name('destroy');
        Route::get('{id}', 'ConsController@show')->name('show');
        Route::post('{id}', 'ConsController@update')->name('update');
        Route::get('{month}/{year}', 'ConsController@folder')->name('folder');
    });

    Route::prefix('contatos')->name('contact.')->group(function () {
        Route::get('/', 'ContactController@index')->name('index');
    });

    Route::prefix('fale-conosco')->name('support.')->group(function () {
        Route::get('/{pp?}', 'SupportController@show')->name('show');
        Route::post('/', 'SupportController@update')->name('update');
    });

});
