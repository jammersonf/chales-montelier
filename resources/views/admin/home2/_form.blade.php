@php
  $file = isset($data) ? [] : ['required'];
@endphp
<div class="col-sm-12 col-md-6">
  <div class="form-group mb-6">
    {!! Form::label('title', 'Título', ['class' => '']) !!}
    {!! Form::text('title', old('title'), ['class' => 'form-control required']) !!}
  </div>
</div>

<div class="col-sm-12 col-md-6">
  <div class="form-group mb-6">
    {!! Form::label('subtitle', 'Subtítulo', ['class' => '']) !!}
    {!! Form::textarea('subtitle', old('subtitle'), ['class' => 'form-control required']) !!}
  </div>
</div>

<div class="col-sm-12 col-md-6">
  <div class="form-group mb-6">
    {!! Form::label('img', 'Img', ['class' => '']) !!}<br>
    @if(isset($data) && $data->img) <img src="{{url('storage/home/'.$data->img)}}" width="50px" height="30px" style="margin-bottom: 5px;"> @endif
    {!! Form::file('img', $file) !!}
  </div>
</div>

<div class="col-sm-12 col-md-6">
  <div class="form-group mb-6">
    {!! Form::label('background', 'Background', ['class' => '']) !!}<br>
    @if(isset($data) && $data->background) <img src="{{url('storage/home/'.$data->background)}}" width="50px" height="30px" style="margin-bottom: 5px;"> @endif
    {!! Form::file('background', $file) !!}
  </div>
</div>
