<div class="icon-holder">
    <div id="icons-wrapper">
        <section>
            <ul>
                <li>
                    <div class="custom-control custom-radio">
                        {!! Form::radio($name, 'icon-cross', old($name), ['class' => 'custom-control-input', 'id' => $name.'-1']) !!}
                        <label class="custom-control-label" for="{{$name}}-1">
                            <i class="icon icon-cross"></i>
                            <p>icon-cross</p>
                        </label>
                    </div>
                </li>
            
                <li>
                    <div class="custom-control custom-radio">
                        {!! Form::radio($name, 'icon-phone', old($name), ['class' => 'custom-control-input', 'id' => $name.'-2']) !!}
                        <label class="custom-control-label" for="{{$name}}-2">
                            <i class="icon icon-phone"></i>
                            <p>icon-phone</p>
                        </label>
                    </div>
                </li>
            
                <li>
                    <div class="custom-control custom-radio">
                        {!! Form::radio($name, 'icon-bars', old($name), ['class' => 'custom-control-input', 'id' => $name.'-3']) !!}
                        <label class="custom-control-label" for="{{$name}}-3">
                            <i class="icon icon-bars"></i>
                            <p>icon-bars</p>
                        </label>
                    </div>
                </li>
            
                <li>
                    <div class="custom-control custom-radio">
                        {!! Form::radio($name, 'icon-mail', old($name), ['class' => 'custom-control-input', 'id' => $name.'-4']) !!}
                        <label class="custom-control-label" for="{{$name}}-4">
                            <i class="icon icon-mail"></i>
                            <p>icon-mail</p>
                        </label>
                    </div>
                </li>
            
                <li>
                    <div class="custom-control custom-radio">
                        {!! Form::radio($name, 'icon-zoom', old($name), ['class' => 'custom-control-input', 'id' => $name.'-5']) !!}
                        <label class="custom-control-label" for="{{$name}}-5">
                            <i class="icon icon-zoom"></i>
                            <p>icon-zoom</p>
                        </label>
                    </div>
                </li>
            
                <li>
                    <div class="custom-control custom-radio">
                        {!! Form::radio($name, 'icon-whats', old($name), ['class' => 'custom-control-input', 'id' => $name.'-6']) !!}
                        <label class="custom-control-label" for="{{$name}}-6">
                            <i class="icon icon-whats"></i>
                            <p>icon-whats</p>
                        </label>
                    </div>
                </li>
            
                <li>
                    <div class="custom-control custom-radio">
                        {!! Form::radio($name, 'icon-plane', old($name), ['class' => 'custom-control-input', 'id' => $name.'-7']) !!}
                        <label class="custom-control-label" for="{{$name}}-7">
                            <i class="icon icon-plane"></i>
                            <p>icon-plane</p>
                        </label>
                    </div>
                </li>
            
                <li>
                    <div class="custom-control custom-radio">
                        {!! Form::radio($name, 'icon-insta', old($name), ['class' => 'custom-control-input', 'id' => $name.'-8']) !!}
                        <label class="custom-control-label" for="{{$name}}-8">
                            <i class="icon icon-insta"></i>
                            <p>icon-insta</p>
                        </label>
                    </div>
                </li>
            
                <li>
                    <div class="custom-control custom-radio">
                        {!! Form::radio($name, 'icon-download', old($name), ['class' => 'custom-control-input', 'id' => $name.'-9']) !!}
                        <label class="custom-control-label" for="{{$name}}-9">
                            <i class="icon icon-download"></i>
                            <p>icon-download</p>
                    </label>
                    </div>
                </li>
            
            <!-- list of icons here with the proper class-->
            </ul>
        </section>
    </div>
</div>

@push('scripts_custom')
    <script>
        function setCollapse(id) {
            $('#'+id.id).collapse('toggle');
        }
    </script>
@endpush
