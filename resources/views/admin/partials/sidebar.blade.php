<div class="main-sidebar sidebar-style-2">
  <aside id="sidebar-wrapper">
    <div class="sidebar-brand mb-2">
      <a href="{{ route('adm.panel') }}"><img src="{{ asset('/assets/img/brand.svg') }}" width="50"></a>
    </div>
    <div class="sidebar-brand sidebar-brand-sm">
      <a href="{{ route('adm.panel') }}"><img src="{{ asset('/assets/img/brand.svg') }}" width="50"></a>
    </div>
    <ul class="sidebar-menu">
      <li class="menu-header"></li>
      <li class="menu-header">Módulos</li>
      <li class="dropdown ">
        <a href="#" class="nav-link has-dropdown">
          <i class=" far fa-star lga"></i>
          <span>Home</span>
        </a>
        <ul class="dropdown-menu" style="display: none;">
          <!--<li>
            <a class=" nav-link" href="route('adm.home.index')" title="Leads">Destaques</a>
          </li> -->
          <li>
            <a class=" nav-link" href="{{route('adm.home2.index')}}" title="Leads">Informações</a>
          </li>
          <li>
            <a class=" nav-link" href="{{route('adm.support.show')}}" title="Leads">Fale conosco</a>
          </li>
          <li>
            <a class=" nav-link" href="{{route('adm.support.show', 'pp')}}" title="Leads">Política de Privacidade</a>
          </li>
        </ul>
      </li>
      <li>
        <a href="{{ route('adm.cons.index')}}">
          <i class="far fa-star lga"></i>
          <span>Obras</span>
        </a>
      </li>
      <li>
        <a href="{{ route('adm.contact.index')}}">
          <i class="far fa-star lga"></i>
          <span>Contatos</span>
        </a>
      </li>
    </ul>
  </aside>
</div>
