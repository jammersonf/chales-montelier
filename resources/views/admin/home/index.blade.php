@extends('admin.layouts.default')

@section('content')

<section class="section">
  <div class="section-header">
    <h1>Home</h1>
      <div class="section-header-button mr-2">
        <a href="{{ route('adm.home.create') }}" class="btn btn-success btn-icon btn-lg btn-success" title="Adicionar"> <i class="fas fa-plus"></i> Adicionar</a>
      </div>
  </div>

  <div class="section-body">
    <div class="row mt-4">
      <div class="col-12">
        <div class="card">
          <div class="card-header">
            <h4>
              <i class="far fa-user lga"></i>
              Listagem <br>
              <small>{{ $data->total() }} resultados.</small>
            </h4>
          </div>
          <div class="card-body -table">
            <div class="table-responsive">
              <table class="table table-striped">
                <tbody>
                  <tr>
                    <th>#</th>
                    <th>Nome</th>
                    <th>Tipo</th>
                    <th>Ações</th>
                  </tr>
                  @foreach($data as $d)
                    <tr>
                      <td>{{ $d->id }}</td>
                      <td>{{ $d->name }}</td>
                      <td>{{ $d->type }}</td>
                      <!--<td>@if($d->img) <img src="storage/home/'.$d->img)" width="50px" style="margin-bottom: 5px;"> @endif</td>-->
                      {!! Form::open(['route' => ['adm.home.destroy', $d->id]]) !!}
                      <td class="no-wrap">
                        <a href="{{ route('adm.home.show', $d->id) }}" class="btn btn-success">Editar</a> | 
                        <button type="submit" class="btn btn-danger" onclick="return confirm('Essa ação não pode ser desfeita!')">Apagar</button>
                      </td>
                      {!! Form::close() !!}
                    </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
          <div class="card-footer">
            <div class="float-right">
              <nav>
                {!! $data->render() !!}
              </nav>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection
