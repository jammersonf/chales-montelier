@extends('layout.default')

@section('navigation')
    @include('components._home_navigation')
@endsection

@section('content')
<div class="container-fluid p-0 ">
    @include('components.360')
    @include('components.empreendimento')
    @include('components.lazer')
    @include('components.downloads')
    {{-- @include('components.obras') --}}
    @include('components.map')
    @include('components.buy')
    @include('components.contact')
</div>
@endsection

@push('scripts')
<script>
    function setCarouselModal(carouselId, number) {
        $('#'+carouselId).carousel(number);
    }

    function scrollLiv(id, dir) {
        console.log('being called with');
        console.log(id);

        if(dir == "right"){
            document.getElementById(id).scrollLeft += 350;
        }else if(dir == "left") {
            document.getElementById(id).scrollLeft -= 350;
        }

        //$('#'+id).scrollLeft = value;
    }

</script>
@endpush
