<div class="sidemenu_360">
    <ul class="pl-0 text-light">
        <li>
            <a href="{{ route('index') }}">
                <img src="{{asset('assets_front/img/logo-dark.svg')}}" class="py-32" width="auto" height="auto" alt=""
                    loading="lazy">
            </a>
        </li>
    </ul>
</div>

<div class="container mx-auto">
    <h1 class="text-dark text-uppercase mb-60 mt-150">Política de Privacidade</h1>
    {!! $support->pc_txt !!}
</div>
