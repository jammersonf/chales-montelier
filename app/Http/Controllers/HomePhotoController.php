<?php

namespace App\Http\Controllers;

use App\Models\Home1;
use App\Models\Home2;
use App\Models\HomePhoto;
use Illuminate\Http\Request;

class HomePhotoController extends Controller
{
    protected $home;
    protected $homePhoto;

    public function __construct(Home2 $home, HomePhoto $hp) {
        $this->middleware('auth');
        $this->home = $home;
        $this->homePhoto = $hp;
    }

    public function index($home)
    {
        $ref = $home;
        $data = $this->home->find($home)
                           ->photos()
                           ->paginate();

        return view('admin.homePhoto.index', compact('data', 'ref'));
    }

    public function create($home)
    {
        $ref = $home;
        return view('admin.homePhoto.create', compact('ref'));
    }

    public function store($home, Request $request)
    {
        $input = $request->all();
        $input['img'] = $this->img($input, 'img', 'home');

        $this->home->find($home)
                   ->photos()
                   ->create($input);

        toastr()->success('Cadastrado com sucesso!');

        return redirect()->route('adm.home2.photo.index', $home);
    }

    public function show($home, $id)
    {
        $ref = $home;
        $data = $this->homePhoto->find($id);

        return view('admin.homePhoto.edit', compact('data', 'ref'));
    }

    public function update($home, $id, Request $request)
    {
        $input = $request->all();
        if (isset($input['img'])) {
            $input['img'] = $this->img($input, 'img', 'home');
        } else {
            unset($input['img']);
        }
        $this->homePhoto->find($id)->update($input);

        toastr()->success('Atualizado com sucesso!');

        return redirect()->route('adm.home2.photo.index', $home);
    }

    public function destroy($home, $id)
    {
        $this->homePhoto->find($id)
                        ->delete();

        toastr()->success('Apagado com sucesso!');

        return redirect()->route('adm.home2.photo.index', $home);
    }
}
