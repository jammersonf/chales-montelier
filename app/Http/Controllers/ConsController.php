<?php

namespace App\Http\Controllers;

use App\Models\Construction;
use Illuminate\Http\Request;

class ConsController extends Controller
{
    protected $construction;

    public function __construct(Construction $construction) {
        $this->middleware('auth')->except('api');
        $this->construction = $construction;
    }

    public function api(Request $request)
    {
        $input = $request->all();
        $data = $this->construction->where('year', $input['year'])
                                   ->where('month', $input['month'])
                                   ->get();

        return response()->json($data, 201);
    }

    public function index()
    {
        $data = $this->construction->groupBy('month', 'year')
                                   ->orderBy('month')
                                   ->paginate();
        
        return view('admin.construction.index', compact('data'));
    }

    public function folder($month, $year)
    {
        session()->put('month', $month);
        session()->put('year', $year);
        $data = $this->construction->where('month', $month)
                                   ->where('year', $year)
                                   ->paginate();
        
        return view('admin.construction.folders', compact('data'));
    }

    public function create()
    {
        return view('admin.construction.create');
    }

    public function store(Request $request)
    {
        $input = $request->all();
        foreach($input['imgs'] as $key => $img) {
            $imageName = time().$key.'.'.$img->extension();
            $img->move(public_path('storage/cons'), $imageName);

            $input['img'] = $imageName;
            $input['title'] = '';

            $this->construction->create($input);
        }

        toastr()->success('Cadastrado com sucesso!');

        return redirect()->route('adm.cons.index');
    }

    public function show($id)
    {
        $data = $this->construction->find($id);

        return view('admin.construction.edit', compact('data'));
    }

    public function update($id, Request $request)
    {
        $input = $request->all();
        if (isset($input['img'])) {
            $input['img'] = $this->img($input, 'img', 'cons');
        } else {
            unset($input['img']);
        }
        $this->construction->find($id)->update($input);

        toastr()->success('Atualizado com sucesso!');

        return redirect()->route('adm.cons.index');
    }

    public function destroy($id)
    {
        $this->construction->find($id)->delete();

        toastr()->success('Apagado com sucesso!');

        return redirect()->route('adm.cons.index');
    }
}
