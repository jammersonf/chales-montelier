<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Home1 extends Model
{
    protected $table = 'home1';

    protected $fillable = [
        'name', 'type', 'url', 'img'
    ];
}
